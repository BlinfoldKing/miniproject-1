defmodule Backend.Router do
  use Plug.Router
  require Logger
  alias Backend.Helper

  plug :match
  plug :dispatch

  get "/issues" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode! get_issues())
  end


  defp get_issues do
    IO.puts Poison.encode! Helper.query
    response = Helper.get_github_issues 
    IO.inspect response 
    Poison.decode! response.body
  end
end
