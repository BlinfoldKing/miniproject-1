defmodule Backend.Schema.Types do
  use Absinthe.Schema.Notation

  object :issue do
    field :title, :string
    field :number, :string
    field :author, :string
    field :body, :string
    field :comments, list_of :comment
  end

  object :comment do
    field :author, :string
    field :body, :string
  end
end
