defmodule Backend.Helper do
  def query do
   %{
     "query" => """
     {
       repository(name: "react", owner: "facebook") {
         issues(last: 50) {
             nodes {
               title
               number
               body
               author {
                 login

               }
               comments(last: 50) {
                 nodes {
                   bodyHTML
                   author {
                     login
                   }
                 }
               }
             }
           }
         }
       }
     """
   }
  end 

  def get_github_issues do
    fetch = Mojito.post(
          "https://api.github.com/graphql",
          [{ "Authorization", "Bearer 7633dc1496f213b85d9c5e79ec2ac69c52269355"}],
          Poison.encode!(query()), 
          []
    )
    case fetch do
      { :ok, response } -> response
      _ -> %{ error: "get github api failed" }
    end
  end
end
