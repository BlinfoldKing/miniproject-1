defmodule Backend.Schema do
  use Absinthe.Schema
  alias Backend.Helper

  import_types Backend.Schema.Types
  
  defp init_database do
    response = Helper.get_github_issues
    body = Poison.decode! response.body
    repo = body["data"]["repository"]   
    Enum.map(
        repo["issues"]["nodes"],
        fn issue -> 
          %{
            title: issue["title"],
            number: issue["number"],
            author: issue["author"]["login"],
            body: issue["body"],
            comments: Enum.map(
              issue["comments"]["nodes"],
              fn comment -> %{
                author: comment["author"]["login"],
                body: comment["bodyHTML"]
              }
              end
            )
          }
        end
      )
  end


  # resolver
  query do
    field :issue, :issue do
      arg :index, non_null :integer
      resolve fn %{ index: index }, _info ->
        database = init_database
        IO.inspect database
        { :ok, Enum.at(database, index) }
      end
    end

    field :issues, list_of :issue do
      resolve fn _params, _info ->
        { :ok, init_database }
      end
    end
  end
end
