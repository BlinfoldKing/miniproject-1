defmodule Backend.Endpoint do
  use Plug.Router

  plug :match
  plug CORSPlug

  plug Plug.Parsers,
    parsers: [:json],
    pass: ["application/json"],
    json_decoder: Poison

  plug :dispatch
  
  def child_spec opts do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts] }
    }
  end

  def start_link(_opts) do
    require Logger
    Logger.info("Starting server at http://localhost:4000/")

    Plug.Cowboy.http(__MODULE__, [])
  end

  forward("/api", to: Backend.Router)
  forward "/graphql", 
    to: Absinthe.Plug.GraphiQL,
    init_opts: [
      schema: Backend.Schema,
      json_codec: Poison
    ],
    interface: :simple


  match _ do
    send_resp(conn, 404, "Requested page not found!")
  end
end
