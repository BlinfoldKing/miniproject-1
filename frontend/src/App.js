import React from 'react';
import './App.css';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: undefined,
      detail: undefined
    }
  }

  async componentDidMount() {
    const response = await fetch('http://localhost:4000/api/issues', {
      // mode: 'no-cors'
    })
    if (response.status === 200) {
      let body = await response.json()
      console.log(body)
      this.setState({ data: body.data.repository.issues.nodes })
    }
  }

  render() {
    const { data, detail } = this.state
    return (
      <div className="App">
          { !detail && data && data.map((d, i) => 
            <div key={i}>
              <h2 onClick={() => {
                this.setState({ detail: d })
              }}>{d.number} - {d.title}</h2>
            </div>
          )}
          {
            detail && (
              <div>
                <h2 onClick={() => this.setState({ detail: undefined })}>{detail.number} - {detail.title}</h2>
                <h3>by {detail.author.login}</h3>
                <div dangerouslySetInnerHTML={{__html: detail.body}}/>
                <h3>comments</h3>
                {detail.comments.nodes.map((c, j) => (
                  <div key={j}>
                    <h4>{c.author.login} says: </h4>
                    <div dangerouslySetInnerHTML={{__html: c.bodyHTML}}/>
                  </div>
                ))}
              </div>
            )
          }
      </div>
    );
  }
}

export default App;
